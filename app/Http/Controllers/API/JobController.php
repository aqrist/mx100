<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Model\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit');
        $name = $request->input('name');

        # code... get single data
        if ($id) {
            # code... for find by id
            $job = Job::find($id);

            if ($job) {
                # code... for sucess
                return ResponseFormatter::success(
                    $job,
                    'Jobs retrieved successfully.'
                );
            } else {
                # code... for error
                return ResponseFormatter::error(
                    null,
                    'Jobs not found.',
                    404
                );
            }
        }

        # code... get all data
        $job = Job::get();
        if ($name) {
            $job->where('name', 'like', '%' . $name . '%');
            if ($job) {
                # code... for sucess
                return ResponseFormatter::success(
                    $job,
                    'Jobs retrieved successfully.'
                );
            } else {
                # code... for error
                return ResponseFormatter::error(
                    null,
                    'Jobs not found.',
                    404
                );
            }
        }

        return ResponseFormatter::success(
            $job,
            'Jobs retrieved successfully.'
        );
    }
}
