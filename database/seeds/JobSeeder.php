<?php

use App\Model\Job;
use Illuminate\Database\Seeder;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Job::create(
            [
                'name' => 'Frontend',
                'description' => 'Frontend job description',
                'status' => 'publish'
            ],
            [
                'name' => 'Backend',
                'description' => 'Backend job description',
                'status' => 'draft'
            ],
        );
    }
}
